package at.badkey.main;


import at.badkey.sorter.Sorter;

public class Main {

	public static void main(String[] args) {
		
		//1000
		Sorter s = new Sorter();
		int[] testArr = s.createArray(1000);
		long time = System.currentTimeMillis();
		s.doSort(testArr);
		System.out.println("1000 items bubble sort took "+(System.currentTimeMillis()-time)+"ms");
		
		//10000
		testArr = s.createArray(10000);
		time = System.currentTimeMillis();
		s.doSort(testArr);
		System.out.println("10000 items bubble sort took "+(System.currentTimeMillis()-time)+"ms");
		
		//100000
		testArr = s.createArray(100000);
		time = System.currentTimeMillis();
		s.doSort(testArr);
		System.out.println("100000 items bubble sort took "+(System.currentTimeMillis()-time)+"ms");
		
		//1000000
		testArr = s.createArray(1000000);
		time = System.currentTimeMillis();
		s.doSort(testArr);
		System.out.println("1000000 items bubble sort took "+(System.currentTimeMillis()-time)+"ms");
	}

}
