package at.badkey.sorter;

public class BubbleSort implements SortingAlgorithm{
	
	@Override
	public int[] doSort(int[] arr) {
		for(int i = 0; i < arr.length; i++) {
			for(int i1 = 1; i1 < arr.length; i1++) {
				if(arr[i1-1] > arr[i1]) {
					int temp = arr[i1];
					arr[i1] = arr[i1-1];
					arr[i1-1] = temp;
				}
			}
		}
		
		return arr;
	}
}
