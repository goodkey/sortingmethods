package at.badkey.sorter;

public interface SortingAlgorithm {
	public int[] doSort(int[] arr);
}
