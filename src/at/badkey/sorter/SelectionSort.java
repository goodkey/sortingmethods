package at.badkey.sorter;

public class SelectionSort implements SortingAlgorithm{

	@Override
	public int[] doSort(int[] arr) {
		int[] sorted = arr;
		
		int i,j;
		
		for( i = 0; i < sorted.length - 1; i++)
		{
			int jMin = i;
			for(j = i+1; j < sorted.length; j++)
			{
				if(sorted[j] < sorted[jMin])
				{
					jMin = j;
				}
			}
			
			if (jMin != i)
			{
				int temp = sorted[i];
				sorted[i] = sorted[jMin];
				sorted[jMin] = temp;
			}
		}
		
		return sorted;
	}

}
