package at.badkey.sorter;

import java.util.Random;

public class Sorter {
	
	SortingAlgorithm a = new BubbleSort();
	
	public void setSortingAlgorithm(SortingAlgorithm sa) {
		this.a = sa;
	}
	
	public void doSort(int[] arr) {
		
		for(int p : a.doSort(arr)) {
			//System.out.println(p);
		}
	}
	
	public int[] createArray(int size) {
		Random r = new Random();
		int[] arrayToSort = r.ints(size,0,10000000).toArray();
		
		return arrayToSort;
	}
 
}
